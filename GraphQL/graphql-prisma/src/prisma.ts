import { Prisma } from 'prisma-binding';

const prisma = new Prisma({
  typeDefs: './src/generated/prisma.graphql',
  endpoint: 'http://localhost:4466'
})

export { prisma as default }

const getUser = async (userId) => {
  const userExits = await prisma.exists.User({ id: userId });

  if(!userExits) {
    throw new Error('User not found');
  }

  return await prisma.query.user({
    where: {
      id: userId
    }
  },  '{ id name email posts { id title published } }');
}

const createPostForUser = async (authorId, data) => {
  const userExits = await prisma.exists.User({ id: authorId });

  if(!userExits) {
    throw new Error('User not found');
  }

  const post = await prisma.mutation.createPost({
    data: {
      ...data,
      author: {
        connect: {
          id: authorId
        }
      }
    }
  }, '{ author { id name email posts { id title published } } }');

  return post;
}

const updatePostForUser = async (postId, data) => {
  const postExists = await prisma.exists.Post({ id: postId });

  if(!postExists) {
    throw new Error('Post not found');
  }

  const post = await prisma.mutation.updatePost({
    where: {
      id: postId
    },
    data
  }, '{ author { id name email posts { id title published } } }');
  return post;
}
