import { GraphQLServer, PubSub } from 'graphql-yoga';
import db from './db';
import QueryResolver from './resolvers/QueryResolver';
import MutationResolver from './resolvers/MutationResolver';
import PostResolver from './resolvers/PostResolver';
import UserResolver from './resolvers/UserResolver';
import CommentResolver from './resolvers/CommentResolver';
import SubscriptionResolver from './resolvers/SubscriptionResolver';
import prisma from './prisma';
import { Context } from './interfaces/context'

// GraphQL 5 Scalar Types: String, Boolean, Int, Float, ID
// exclaimation mark (!) means that the returns object cannot be null

const pubsub: PubSub = new PubSub();

const resolvers = {
  Query: QueryResolver,
  Mutation: MutationResolver,
  Subscription: SubscriptionResolver,
  Post: PostResolver,
  User: UserResolver,
  Comment: CommentResolver,
};

const server: GraphQLServer = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    db,
    pubsub,
    prisma
  } as Context,
});

server.start(() => {
  console.log('The server is up');
});
