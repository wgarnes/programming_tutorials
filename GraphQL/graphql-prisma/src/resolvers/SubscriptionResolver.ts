import { Context } from '../interfaces/context';
import { Post } from '../interfaces/post';

const SubscriptionResolver = {
  comment: {
    subscribe(parent, args: { postId: 'string' }, ctx: Context, info) {
      const getPost: Post = ctx.db.posts.find((post: Post) => post.id === args.postId && post.published);

      if (!getPost) {
        throw new Error('Post not found');
      }

      return ctx.pubsub.asyncIterator(`comment ${args.postId}`);
    },
  },
  post: {
    subscribe(parent, args, ctx: Context, info) {
      return ctx.pubsub.asyncIterator('post');
    },
  },
};

export { SubscriptionResolver as default };
