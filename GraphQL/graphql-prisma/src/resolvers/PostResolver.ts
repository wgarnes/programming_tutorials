import { User } from '../interfaces/user';
import { Context } from '../interfaces/context';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';

const PostResolver = {
  author(parent: Post, args, ctx: Context, info): User {
    return ctx.db.users.find((user: User) => user.id === parent.author);
  },
  comments(parent: Post, args, ctx: Context, info): Comment[] {
    return ctx.db.comments.filter((comment: Comment) => comment.post === parent.id);
  },
};

export { PostResolver as default };
