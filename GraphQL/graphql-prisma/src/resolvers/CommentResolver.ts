import { User } from '../interfaces/user';
import { Context } from '../interfaces/context';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';

const CommentResolver = {
  author(parent: Comment, args, ctx: Context, info): User {
    return ctx.db.users.find((user: User) => user.id === parent.author);
  },
  post(parent: Comment, args, ctx: Context, info) {
    return ctx.db.posts.find((post: Post) => post.id === parent.post);
  },
};

export { CommentResolver as default };
