interface NewComment {
  text: string;
  author: string;
  post: string;
}

interface Comment extends NewComment {
  id: string;
}

interface UpdateComment {
  id: string;
  text: string;
}

function comment(id: string, newComment: NewComment): Comment;
function comment(id: string, text: string, author: string, post: string): Comment;

function comment(paramOne, paramTwo, paramThree?, paramFour?): Comment {
  if (typeof paramTwo === 'object') {
    return { id: paramOne, ...paramTwo };
  } else {
    return { id: paramOne, text: paramTwo, author: paramThree, post: paramFour };
  }
}

export { NewComment, Comment, UpdateComment, comment };
