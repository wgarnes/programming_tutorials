import { Database } from './database';
import { PubSub } from 'graphql-yoga';
import { Prisma } from 'prisma-binding';

interface Context {
  db: Database;
  pubsub: PubSub;
  prisma: Prisma
}

export { Context };
