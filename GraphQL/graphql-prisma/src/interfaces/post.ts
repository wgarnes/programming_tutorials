interface NewPost {
  title: string;
  body: string;
  published: boolean;
  author: string;
}

interface Post extends NewPost {
  id: string;
}

interface UpdatePost {
  id: string;
  title?: string;
  body?: string;
  published?: boolean;
}

function post(id: string, title: string, body: string, published: boolean, author: string): Post;
function post(id: string, newPost: NewPost): Post;

function post(paramOne, paramTwo, paramThree?, paramFour?, paramFive?): Post {
  if (typeof paramTwo === 'object') {
    return { id: paramOne, ...paramTwo };
  } else {
    return { id: paramOne, title: paramTwo, body: paramThree, published: paramFour, author: paramFive };
  }
}

export { NewPost, Post, UpdatePost, post };
