import { Post, post } from './interfaces/post';
import { User, user } from './interfaces/user';
import { Comment, comment } from './interfaces/comment';
import { Database } from './interfaces/database';

const users: User[] = [
  user('1', 'William', 'william@name.com', 25),
  user('2', 'Bob', 'bob@name.com', 3),
  user('3', 'Susan', 'susan@name.com'),
];

const posts: Post[] = [
  post('1', 'run', 'run forest run', true, '1'),
  post('2', 'chicken', 'eat more chicken', false, '1'),
  post('3', 'yummy', 'yum yum give me some', true, '2'),
];

const comments: Comment[] = [
  comment('1', 'this is funny', '2', '1'),
  comment('2', 'repeat something said in article', '3', '2'),
  comment('3', 'this comment was deleted', '3', '1'),
  comment('4', 'comment numba foa', '2', '3'),
];

const db: Database = {
  users,
  posts,
  comments,
};

export { db as default, Database };
