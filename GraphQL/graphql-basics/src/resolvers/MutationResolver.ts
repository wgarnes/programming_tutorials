import { v4 as uuidv4 } from 'uuid';
import { User, NewUser, UpdateUser, user } from '../interfaces/user';
import { Context } from '../interfaces/context';
import { Post, NewPost, UpdatePost, post } from '../interfaces/post';
import { Comment, NewComment, UpdateComment, comment } from '../interfaces/comment';
import { PubSub } from 'graphql-yoga';

enum MutationType {
  CREATED = 'CREATED',
  UPDATED = 'UPDATED',
  DELETED = 'DELETED',
}

const MutationResolver = {
  createUser(parent, args: { data: NewUser }, ctx: Context, info): User {
    const emailTaken: boolean = ctx.db.users.some((user) => user.email === args.data.email);
    if (emailTaken) {
      throw new Error('Email taken.');
    }

    const newUser: User = user(uuidv4(), args.data);
    ctx.db.users.push(newUser);
    return newUser;
  },
  deleteUser(parent, args: { id: string }, ctx: Context, info): User {
    const userIndex: number = ctx.db.users.findIndex((user: User) => user.id === args.id);
    if (userIndex === -1) {
      throw new Error('User not found');
    }

    const deletedUser: User = ctx.db.users.splice(userIndex, 1)[0];
    ctx.db.posts = ctx.db.posts.filter((post: Post) => {
      const match = post.author === args.id;

      if (match) {
        ctx.db.comments = ctx.db.comments.filter((comment: Comment) => comment.post !== post.id);
      }

      return !match;
    });

    ctx.db.comments = ctx.db.comments.filter((comment: Comment) => comment.author !== args.id);
    return deletedUser;
  },
  updateUser(parent, args: { data: UpdateUser }, ctx: Context, info): User {
    const { data } = args;
    const userUpdate: User = ctx.db.users.find((user: User) => user.id === data.id);
    if (userUpdate === undefined) {
      throw new Error('User not found');
    }

    if (typeof data.email === 'string') {
      const emailTaken = ctx.db.users.some((user: User) => user.email === data.email);
      if (emailTaken) {
        throw new Error('Email is taken');
      }
      userUpdate.email = data.email;
    }

    if (typeof data.name === 'string') {
      userUpdate.name = data.name;
    }

    if (typeof data.age !== 'undefined') {
      userUpdate.age = data.age;
    }

    return userUpdate;
  },
  createPost(parent, args: { data: NewPost }, ctx: Context, info): Post {
    const userExists: boolean = ctx.db.users.some((user) => user.id === args.data.author);

    if (!userExists) {
      throw new Error('User not found.');
    }

    const newPost: Post = post(uuidv4(), args.data);
    ctx.db.posts.push(newPost);
    if (args.data.published) {
      ctx.pubsub.publish(`post`, {
        post: {
          mutation: MutationType.CREATED,
          data: newPost,
        },
      });
    }
    return newPost;
  },
  deletePost(parent, args: { id: string }, ctx: Context, info): Post {
    const postId: string = args.id;
    const postIndex: number = ctx.db.posts.findIndex((post: Post) => post.id === postId);

    if (postIndex === -1) {
      throw new Error('Post not found');
    }

    ctx.db.comments = ctx.db.comments.filter((comment: Comment) => comment.post !== postId);
    const deletedPost = ctx.db.posts.splice(postIndex, 1)[0];

    if (deletedPost.published) {
      ctx.pubsub.publish('post', {
        post: {
          mutation: MutationType.DELETED,
          data: deletedPost,
        },
      });
    }
    return deletedPost;
  },
  updatePost(parent, args: { data: UpdatePost }, ctx: Context, info): Post {
    const { db } = ctx;
    const { data } = args;
    const postUpdate: Post = db.posts.find((post: Post) => post.id === data.id);
    const originalPost: Post = { ...postUpdate };
    if (postUpdate === undefined) {
      throw new Error('Post not found');
    }

    const update = (field: string, type: string) => {
      if (typeof data[field] === type) {
        postUpdate[field] = data[field];
      }
    };

    update('body', 'string');
    update('published', 'boolean');
    update('title', 'string');

    const publish = (mutation: string, data: Post) => {
      ctx.pubsub.publish('post', {
        post: {
          mutation,
          data,
        },
      });
    };

    if (originalPost.published && !postUpdate.published) {
      publish(MutationType.DELETED, originalPost);
    } else if (postUpdate.published && !originalPost.published) {
      publish(MutationType.CREATED, postUpdate);
    } else if (postUpdate.published) {
      publish(MutationType.UPDATED, postUpdate);
    }

    return postUpdate;
  },
  createComment(parent, args: { data: NewComment }, ctx: Context, info): Comment {
    const userExists: boolean = ctx.db.users.some((user) => user.id === args.data.author);
    if (!userExists) {
      throw new Error('User not found.');
    }

    const commentPost: Post = ctx.db.posts.find((post) => post.id === args.data.post);
    if (commentPost === undefined || !commentPost.published) {
      throw new Error('Post not found.');
    }

    const newComment: Comment = comment(uuidv4(), args.data);
    ctx.db.comments.push(newComment);

    ctx.pubsub.publish(`comment ${args.data.post}`, {
      comment: {
        mutation: MutationType.CREATED,
        data: newComment,
      },
    });
    return newComment;
  },
  deleteComment(parent, args: { id: string }, ctx: Context, info): Comment {
    const commentIndex: number = ctx.db.comments.findIndex((comment: Comment) => comment.id === args.id);
    if (commentIndex === -1) {
      throw new Error('Could not find comment');
    }

    const deletedComment: Comment = ctx.db.comments.splice(commentIndex, 1)[0];

    ctx.pubsub.publish(`comment ${deletedComment.post}`, {
      comment: {
        mutation: MutationType.DELETED,
        data: deletedComment,
      },
    });

    return deletedComment;
  },
  updateComment(parent, args: { data: UpdateComment }, ctx: Context, info): Comment {
    const { db } = ctx;
    const { data } = args;
    const commentUpdate: Comment = db.comments.find((comment: Comment) => comment.id === data.id);

    if (commentUpdate === undefined) {
      throw new Error('Comment not found');
    }

    if (typeof data.text === 'string') {
      commentUpdate.text = data.text;
    }

    ctx.pubsub.publish(`comment ${commentUpdate.post}`, {
      comment: {
        mutation: MutationType.UPDATED,
        data: commentUpdate,
      },
    });

    return commentUpdate;
  },
};

export { MutationResolver as default };
