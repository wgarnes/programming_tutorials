import { User } from '../interfaces/user';
import { Context } from '../interfaces/context';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';

const QueryResolver = {
  users(parent, args: { query: string }, ctx: Context, info): User[] {
    if (!args.query) {
      return ctx.db.users;
    }

    return ctx.db.users.filter((user: User) => {
      return user.name.toLowerCase().includes(args.query.toLowerCase());
    });
  },
  posts(parent, args: { query: string }, ctx: Context, info): Post[] {
    if (!args.query) {
      return ctx.db.posts;
    }

    const query: string = args.query.toLowerCase();
    return ctx.db.posts.filter((post: Post) => {
      return post.title.toLowerCase().includes(query) || post.body.toLowerCase().includes(query);
    });
  },
  me(): User {
    return {
      id: '123098',
      name: 'Mike',
      email: 'mike@example.com',
      age: 25,
    };
  },
  post(): Post {
    return {
      id: '123abc',
      title: 'I like chicken',
      body: "don't eat the beef",
      published: false,
      author: '1',
    };
  },
  comments(parent, args, ctx: Context, info): Comment[] {
    return ctx.db.comments;
  },
};

export { QueryResolver as default };
