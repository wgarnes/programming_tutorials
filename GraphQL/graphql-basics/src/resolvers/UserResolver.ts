import { User } from '../interfaces/user';
import { Context } from '../interfaces/context';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';

const UserResolver = {
  posts(parent: User, args, ctx: Context, info): Post[] {
    return ctx.db.posts.filter((post: Post) => post.author === parent.id);
  },
  comments(parent: User, args, ctx: Context, info): Comment[] {
    return ctx.db.comments.filter((comment: Comment) => comment.author === parent.id);
  },
};

export { UserResolver as default };
