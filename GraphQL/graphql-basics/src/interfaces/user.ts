interface NewUser {
  name: string;
  email: string;
  age?: number;
}

interface User extends NewUser {
  id: string;
}

interface UpdateUser {
  id: string;
  name?: string;
  email?: string;
  age?: number;
}

function user(id: string, name: string, email: string, age?: number): User;
function user(id: string, newUser: NewUser): User;

function user(paramOne, paramTwo, paramThree?, paramFour?): User {
  if (typeof paramTwo === 'object') {
    return { id: paramOne, ...paramTwo };
  } else {
    return { id: paramOne, name: paramTwo, email: paramThree, age: paramFour };
  }
}

export { NewUser, User, UpdateUser, user };
