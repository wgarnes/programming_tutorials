import { User } from './user';
import { Post } from './post';
import { Comment } from './comment';

interface Database {
  users: User[];
  posts: Post[];
  comments: Comment[];
}

export { Database };
