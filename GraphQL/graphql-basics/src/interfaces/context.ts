import { Database } from './database';
import { PubSub } from 'graphql-yoga';

interface Context {
  db: Database;
  pubsub: PubSub;
}

export { Context };
