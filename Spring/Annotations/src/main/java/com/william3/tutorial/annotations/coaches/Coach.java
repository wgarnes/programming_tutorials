package com.william3.tutorial.annotations.coaches;

public interface Coach {
    public String getDailyWorkout();

    public String getDailyFortune();
}
