package com.william3.tutorial.annotations.fortuneServices;

import org.springframework.stereotype.Component;

@Component
public class RestFortuneService implements FortuneService {
    @Override
    public String getFortune() {
        return "this is a rest service";
    }
}
