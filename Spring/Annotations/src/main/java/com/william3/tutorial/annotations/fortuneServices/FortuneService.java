package com.william3.tutorial.annotations.fortuneServices;

public interface FortuneService {

    public String getFortune();
}
