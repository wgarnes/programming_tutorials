package com.william3.tutorial.annotations.configFiles;

import com.william3.tutorial.annotations.coaches.Coach;
import com.william3.tutorial.annotations.coaches.TrackCoach;
import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import com.william3.tutorial.annotations.fortuneServices.SadFortuneService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/*
The @Configuration annotation will allow you to create beans of different classes without using the @Component keyword

The @Bean annotation will turn the returned object into a bean whose name is equal to the name of the function.  The
    first time the function is called it will construct to object.  On each sequential call the function will return
    a reference to the object that was originally created on the first call
 */

@Configuration
@PropertySource("classpath:application.properties")
public class SportConfigNoScan {

    @Bean
    public FortuneService sadFortuneService() {
        return new SadFortuneService();
    }

    @Bean
    public Coach trackCoach() {
        return new TrackCoach(sadFortuneService());
    }
}
