package com.william3.tutorial.annotations.coaches;

import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/* Annotation Notes

the @Component annotation will create a bean of the class, the name of the bean is the same as the class, but with
    the first letter of the class is lower case. ex: SwimCoach bean name is swimCoach

using @Autowired on the constructor is 1 of 4 ways to set beans. The program will scan the project for beans
    that match the classes passed through the constructor and set the parameters as the beans

when there are several beans of the same base class the @Qualifier annotation is used to specify the bean by using
    the name of the bean

the @PostConstruct keyword will trigger after the object has been completely constructed which includes all @Autowired
    and @Value properties being set

the @PreDestroy keyword will trigger right before the object is destroyed.  If the scope of a bean is set as prototype
    then the @PreDestroy annotation will not trigger
*/

@Component
public class SwimCoach implements Coach {

    private final FortuneService fortuneService;

    @Autowired
    public SwimCoach(@Qualifier("restFortuneService") FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "swim 5 miles";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    @PostConstruct
    public void init() {
        System.out.println("init for swim coach");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy for swim coach");
    }
}
