package com.william3.tutorial.annotations.fortuneServices;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomFortuneService implements FortuneService {

    private final String data[] = {
      "Don't look down",
      "Your feet are smelly",
      "You are dumb"
    };

    private final Random random = new Random();
    @Override
    public String getFortune() {
        return data[random.nextInt(3)];
    }
}
