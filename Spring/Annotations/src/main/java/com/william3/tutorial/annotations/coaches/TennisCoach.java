package com.william3.tutorial.annotations.coaches;

import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


/* Annotation Notes

using @Autowired on a property is 1 of 4 ways to set beans. The program will scan the project for beans
    that match the class and set the property as that bean

the @Scope annotation is used to set the uniqueness of the beans.  The options for scope are prototype and singleton.
    If the scope is not set then a bean will default to a singleton. The word singleton means that each call for the
    bean will all receive a reference to the same object.
*/

@Component
@Scope("singleton")
public class TennisCoach implements Coach {

    @Autowired
    @Qualifier("randomFortuneService")
    private FortuneService fortuneService;

    @Override
    public String getDailyWorkout() {
        return "Practice your backhand volley";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    @PostConstruct
    public void init() {
        System.out.println("init for tennis coach");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy for tennis coach");
    }
}
