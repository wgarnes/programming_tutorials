package com.william3.tutorial.annotations.configFiles;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

/* Annotation Notes

using the @ComponentScan annotation will allow for the scanning of the program to create beans without have to create
    a XML file

using the @PropertySource keyword gives the location of the properties files to be used by spring
*/

@ComponentScan("com.william3.tutorial.annotations")
@PropertySource("classpath:application.properties")
public class SportConfigScan {
}
