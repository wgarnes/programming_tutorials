package com.william3.tutorial.annotations.coaches;

import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/* Annotation Notes

using @Autowired on a setter method is 1 of 4 ways to set beans. The program will scan the project for beans
    that match the classes passed through the method and set the parameters as the beans

the @Scope annotation is used to set the uniqueness of the beans.  The options for scope are prototype and singleton.
    If the scope is not set then a bean will default to a singleton. The word prototype means that each call for the
    bean will get a unique instance of the class.  If a bean is a prototype then @PreDestroy annotation will
    not be triggered
*/

@Component
@Scope("prototype")
public class FootballCoach implements Coach {

    FortuneService fortuneService;

    @Override
    public String getDailyWorkout() {
        return "tackle dem bois";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    @Autowired
    public FootballCoach setFortuneService(@Qualifier("happyFortuneService") FortuneService fortuneService) {
        this.fortuneService = fortuneService;
        return this;
    }

    @PostConstruct
    public void init() {
        System.out.println("init for football coach");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy for football coach");
    }
}
