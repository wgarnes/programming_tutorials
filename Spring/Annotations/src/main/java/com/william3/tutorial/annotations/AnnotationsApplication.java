package com.william3.tutorial.annotations;

import com.william3.tutorial.annotations.coaches.Coach;
import com.william3.tutorial.annotations.configFiles.SportConfigNoScan;
import com.william3.tutorial.annotations.configFiles.SportConfigScan;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

// @SpringBootApplication
public class AnnotationsApplication {

	private static void annotationBasics() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// the name of the bean is the name of the class starting with a lower case
		Coach theCoach = context.getBean("swimCoach", Coach.class);

		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFortune());

		context.close();
	}

	private static void beanScope() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		String[] beanNames = {"swimCoach", "tennisCoach", "footballCoach"};

		for (String beanName : beanNames) {
			Coach theCoach1 = context.getBean(beanName, Coach.class);
			Coach theCoach2 = context.getBean(beanName, Coach.class);

			boolean equal = theCoach1 == theCoach2;
			System.out.printf("Information about bean %s\n", beanName);
			System.out.println("Pointing to the same object: " + equal);
			System.out.println("Address to theCoach1: " + theCoach1);
			System.out.println("Address to theCoach2: " + theCoach2);
			System.out.println();
		}
	}

	private static void javaConfigScan() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfigScan.class);

		Coach theCoach = context.getBean("soccerCoach", Coach.class);

		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFortune());

		context.close();
	}

	private static void javaConfigBeans() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfigNoScan.class);

		Coach theCoach = context.getBean("trackCoach", Coach.class);

		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFortune());

		context.close();
	}

	private static void run(Integer option) {
		switch (option) {
			case 1:
				annotationBasics();
				break;
			case 2:
				beanScope();
				break;
			case 3:
				javaConfigScan();
				break;
			case 4:
				javaConfigBeans();
				break;
			default:
				System.out.println("The is not an option");
				break;
		}
	}

	private static void run() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("1. Creating Beans using the component annotation and XML configuration file");
		System.out.println("2. Bean scopes for prototypes and singletons");
		System.out.println("3. Creating Beans using the component annotation and a java config class that scans");
		System.out.println("4. Creating Beans using the a java config class that manually creates the beans");
		System.out.print("Select Example to View: ");
		run(scanner.nextInt());
	}

	public static void main(String[] args) {
		//SpringApplication.run(AnnotationsApplication.class, args);
		run();
	}

}
