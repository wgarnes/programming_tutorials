package com.william3.tutorial.annotations.coaches;

import com.william3.tutorial.annotations.fortuneServices.DatabaseFortuneService;
import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/* Annotation Notes

using @Autowired on a random function is 1 of 4 ways to set beans. The program will scan the project for beans
    that match the classes passed through the method and set the parameters as the beans
*/

@Component
public class SoccerCoach implements Coach {
    FortuneService fortuneService;

    @Override
    public String getDailyWorkout() {
        return "kick that ball";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    @Autowired
    public void randomFunctionName(DatabaseFortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @PostConstruct
    public void init() {
        System.out.println("init for soccer coach");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy for soccer coach");
    }
}
