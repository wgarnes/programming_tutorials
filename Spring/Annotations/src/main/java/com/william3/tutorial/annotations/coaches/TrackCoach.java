package com.william3.tutorial.annotations.coaches;

import com.william3.tutorial.annotations.fortuneServices.FortuneService;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/* Annotation Notes
The value keyword is used to assign a property a value upon its construction.  When the syntax ${} is used, it will
    get the value from the properties file.  Make sure the configuration file has set the property source to get the
    property files values
*/

public class TrackCoach implements Coach {
    private FortuneService fortuneService;

    @Value("${coach.name}")
    private String name;

    @Value("${coach.email}")
    private String email;

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "run a mile";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    @PostConstruct
    private void init() {
        System.out.println("Coach name is " + name);
    }

    @PreDestroy
    private void destroy() {
        System.out.println("Coach email is " + email);
    }
}
