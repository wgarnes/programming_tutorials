package com.william3.tutorial.annotations.fortuneServices;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

@Component
public class DatabaseFortuneService implements FortuneService {

    private final ArrayList<String> data = new ArrayList<>();

    private final Random random = new Random();

    @Value("${data.filename}")
    private String fileName;

    @PostConstruct
    private void fillDatabase(){
        File file = new File(fileName);

        try (Scanner scanner = new Scanner(file, StandardCharsets.UTF_8.name())){
            while(scanner.hasNextLine()){
                data.add(scanner.nextLine());
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.println("The size of the database is " + data.size());
    }

    @Override
    public String getFortune() {
        return data.get(random.nextInt(data.size()));
    }
}
