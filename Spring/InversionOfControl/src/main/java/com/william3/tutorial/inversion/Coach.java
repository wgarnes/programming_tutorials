package com.william3.tutorial.inversion;

public interface Coach {
    public String getDailyWorkout();

    public String getDailyFortune();

}
