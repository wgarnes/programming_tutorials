package com.william3.tutorial.inversion;

public interface FortuneService {

    public String getFortune();
}
