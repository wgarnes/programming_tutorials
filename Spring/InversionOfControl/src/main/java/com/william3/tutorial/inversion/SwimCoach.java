package com.william3.tutorial.inversion;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


@NoArgsConstructor
@Setter
@Accessors(chain = true)
public class SwimCoach implements Coach {
    private FortuneService fortuneService;

    @Override
    public String getDailyWorkout() {
        return "swim 5 miles";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
