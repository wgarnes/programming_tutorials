package com.william3.tutorial.inversion;

import org.springframework.context.support.ClassPathXmlApplicationContext;

//@SpringBootApplication
public class DemoApplication {

	private static void beanDemo() {
		// load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// get the bean from the container and print info
		printCoachInfo(context.getBean("myCoach", Coach.class));
		printCoachInfo(context.getBean("myCricketCoach", CricketCoach.class));
		printCoachInfo(context.getBean("swimCoach", Coach.class));

		// close the context
		context.close();
	}

	private static void beanScopeDemo() {
		// load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");

		// get the bean from the container and print info
		Coach theCoach1 = context.getBean("myCoach", Coach.class);
		Coach theCoach2 = context.getBean("myCoach", Coach.class);

		System.out.println("Pointing at the same object: " + (theCoach1 == theCoach2));
		System.out.println("Memory Location for theCoach1: " + theCoach1);
		System.out.println("Memory Location for theCoach2: " + theCoach2);
		System.out.println();
		// close the context
		context.close();
	}

	private static void beanLifeCycle() {
		// load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle-applicationContext.xml");

		Coach coach = context.getBean("myCoach", Coach.class);

		System.out.println(coach.getDailyWorkout());

		context.close();
	}

	private static void printCoachInfo(Coach coach) {
		System.out.println(coach.getDailyWorkout());
		System.out.println(coach.getDailyFortune());
		System.out.println();
	}

	private static void printCoachInfo(CricketCoach coach) {
		System.out.println(coach.getDailyFortune());
		System.out.println(coach.getDailyWorkout());
		System.out.println(coach.getEmailAddress());
		System.out.println(coach.getTeam());
		System.out.println();
	}

	public static void main(String[] args) {
		beanDemo();
		beanScopeDemo();
		beanLifeCycle();
		//SpringApplication.run(DemoApplication.class, args);
	}

}
