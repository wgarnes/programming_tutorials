package com.william3.tutorial.inversion;

import java.util.Random;

public class RandomFortunesService implements FortuneService {

    private final String fortunes[] = { "Don't go home today", "Tomorrow is going to be great", "Don't look down" };
    private final Random random = new Random();

    @Override
    public String getFortune() {
        return fortunes[random.nextInt(3)];
    }
}
