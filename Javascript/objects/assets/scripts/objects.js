/*const movieList = document.getElementById('movie-list')
movieList.style.display = 'block'
movieList.style['background-color'] = 'red'

const userChosenKeyName = 'level'

const person = {
  name: {
    first: 'Manto',
    last: 'Kintamson'
  },
  age: -2,
  hobbies: ['Fighting', 'Saving Lives'],
  [userChosenKeyName]: 'wow',
  greet: function() {
    alert('Hi there!')
  },
  1.5: 'hello'
}

person.isAdmin = true
delete person.age
console.log(person)
console.log(person[1.5])*/

const addMovieBtn = document.getElementById('add-movie-btn')
const searchBtn = document.getElementById('search-btn')

const movies = []

const renderMovies = (filter = '') => {
  const movieList = document.getElementById('movie-list')

  if (!movies.length) {
    movieList.classList.remove('visible')
    return
  }
  movieList.innerHTML = ''
  movieList.classList.add('visible')

  const filteredMovies = !filter ? movies : movies.filter(movie => movie.info.title.includes(filter))
  for (const movie of filteredMovies) {
    const { info } = movie
    const movieEl = document.createElement('li')
    const [key, value] = Object.entries(info).find(([key]) => key != 'title' && key != 'title' )
    // const { title: movieTitle } = info
    let { getFormattedTitle } = movie
    // getFormattedTitle = getFormattedTitle.bind(movie)
    movieEl.textContent = `${ movie.getFormattedTitle.call(movie) } - ${key}: ${value}`;
    movieList.appendChild(movieEl)
  }
}

const addMovieHandler = () => {
  const title = document.getElementById('title').value
  const extraName = document.getElementById('extra-name').value
  const extraValue = document.getElementById('extra-value').value

  if (!title.trim() || !extraName.trim() || !extraValue.trim()) {
    return
  }

  const newMovie = {
    info: {
      set title(value) {
        this._title = value
      },
      get title() {
        return this._title
      },
      [extraName]: extraValue
    },
    id: Math.random(),
    getFormattedTitle() {
      return this.info.title.toUpperCase()
    }
  }
  newMovie.info.title = title
  movies.push(newMovie)
  console.log(newMovie)
  renderMovies()
};

const searchMovieHandler = () => {
  const filterTerm = document.getElementById('filter-title').value
  renderMovies(filterTerm)
}

addMovieBtn.addEventListener('click', addMovieHandler)
searchBtn.addEventListener('click', searchMovieHandler)
