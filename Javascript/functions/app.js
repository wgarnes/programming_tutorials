const startGameBtn = document.getElementById('start-game-btn')

const ROCK = 'ROCK'
const PAPER = 'PAPER'
const SCISSORS = 'SCISSORS'
const RESULT = {
  DRAW: 'DRAW',
  COMPUTER_WINS: 'COMPUTER_WINS',
  PLAYER_WINS: 'PLAYER_WINS'
}
const DEFAULT_USER_CHOICE = ROCK
const options = new Set([ROCK, PAPER, SCISSORS])

let gameIsRunning = false

const getPlayerChoice = function () {
  const selection = prompt(`${ROCK}, ${PAPER}, or ${SCISSORS}?`, '').toUpperCase()
  if (!options.has(selection)) {
    alert(`Invalid choice! We chose ${DEFAULT_USER_CHOICE} for you!`)
    return
  }

  return selection
}

const getWinner = function(computerChoice, playerSelection = DEFAULT_USER_CHOICE) {
  const winOptions = {
    ROCK: SCISSORS,
    SCISSORS: PAPER,
    PAPER: ROCK
  }

  if(computerChoice === playerSelection) {
    return RESULT.DRAW
  } else if (winOptions[computerChoice] === playerSelection) {
    return RESULT.COMPUTER_WINS
  } else {
    return RESULT.PLAYER_WINS
  }
}

const getComputerChoice = function() {
  const randomValue = Math.random() * 3
  if(randomValue < 1) {
    return ROCK
  } else if (randomValue < 2) {
    return PAPER
  } else {
    return SCISSORS
  }
}

const startGame = function() {
  if (gameIsRunning) {
    return
  }
  gameIsRunning = true
  console.log('Game is starting...')
  const playerSelection = getPlayerChoice()
  const computerChoice = getComputerChoice()
  let winner
  if (playerSelection) {
    winner = getWinner(computerChoice, playerSelection)
  } else {
    winner = getWinner(computerChoice, playerSelection)
  }
  let message = `You picked ${playerSelection || DEFAULT_USER_CHOICE}, computer picked ${computerChoice}, therefore you `
  if (winner == RESULT.DRAW) {
    message += 'had a draw'
  } else if (winner == RESULT.PLAYER_WINS) {
    message += 'won'
  } else {
    message += 'lost'
  }
  alert(message)
  gameIsRunning = false
}

startGameBtn.addEventListener('click', startGame)

const sumUp = (resultHandler, ...numbers) => {
  let sum = 0
  for (const num of numbers) {
    sum += num
  }
  resultHandler(sum)
  return sum
}

const subtractUp = function(resultHandler) {
  const validateNumber = number => isNaN(number) ? 0 : number

  let sum = 0
  for (const num of arguments) {
    sum -= validateNumber(num)
  }

  resultHandler(sum)

  return sum
}

const showResult = (messageText, result) =>  {
  alert(`${messageText} ${result}`)
}

console.log(sumUp(showResult.bind(this, 'The result after adding all numbers is:'), 1, 5, 10, -3, 6, 10))
console.log(sumUp(showResult.bind(this, 'The result after adding all numbers is:'), 1, 5, 10, -3, 6, 10, 25, 88))
console.log(subtractUp(showResult.bind(this, 'The result after subtracting all numbers is:'), 1, 10, 15, 20));
