const ids = new Set([1, 2, 3])
console.log(ids.has(2))
ids.add(2)
ids.delete(-2)
console.log(ids)

for (const entry of ids.entries()) {
  console.log(entry[0])
}