const masters = [['manto', 'movement'], ['kintamson', 'magic'], ['blink', 'speed']]
const mastersMap = new Map(masters)

mastersMap.set('junikiko', 'knowledge')
console.log(mastersMap)
console.log(mastersMap.get('manto'))

for (const [key, value] of mastersMap.entries()) {
  console.log(key, value)
}