let numbers = [1, 2, 3]
console.log(numbers)

/*const strings = new Array('Hi', 'Welcome')
console.log(strings);

const moreNumbers = Array(5)
console.log(moreNumbers);

const yetMoreNumbers = Array.of(1, 2)
console.log(yetMoreNumbers);

const listItems = document.getElementsByTagName('li')
console.log(listItems)

const arrayListItems = Array.from(listItems)
console.log(arrayListItems)*/

numbers = Array.from(Array(10), (_, index) => index + 1)
console.log(numbers)
numbers.push(11) // adds to the end
console.log(numbers)
numbers.unshift(0) // adds to the beginning
console.log(numbers)
numbers.shift() // removes from the beginning
console.log(numbers)
let numbers2 = numbers.slice() // makes a copy of the array
console.log(numbers2)
numbers2 = numbers.slice(1,5) // make a copy of array from index [1, 5)
console.log(numbers2);
numbers2 = numbers2.concat([11, 12, 13, 14])
console.log(numbers2);
console.log(numbers2.includes(13))

const prices = [10.99, 5.99, 3.99, 6.59]
prices.sort((num1, num2) => num1 - num2)
console.log(prices)
prices.reverse()
console.log(prices)

const sum = prices.reduce((previousVal, currentVal) => previousVal + currentVal, 0)
console.log(sum)

console.log(Math.min(...prices))

const nameData = ['Bob', 'Jones', 'Hey', 'Please', 'Listen']
const [firstName, lastName, ...otherNames] = nameData
console.log(firstName, lastName, otherNames)
console.log(nameData.join())
console.log(nameData.join(' '))
console.log('split-on-the-dashes'.split('-'))