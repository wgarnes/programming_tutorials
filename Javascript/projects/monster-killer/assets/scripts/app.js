const ATTACK_VALUE = 10
const STRONG_ATTACK_VALUE = 17
const MONSTER_ATTACK_VALUE = 14
const HEAL_VALUE = 20

let chosenMaxLife

try {
  chosenMaxLife = getChosenMaxLife()
} catch (error) {
  console.log(error)
  chosenMaxLife = 100
}

let currentMonsterHealth = chosenMaxLife
let currentPlayerHealth = chosenMaxLife
let hasBonusLife = true
let battleLog = []

const writeToLog = (event, value, monsterHealth, playerHealth, target) => {
  const logEntry = { event, value, monsterHealth, playerHealth }
  if (target) {
    logEntry.target = target
  }
  battleLog.push(logEntry)
}

const reset = (result) => {
  writeToLog('GAME_OVER', result, currentMonsterHealth, currentPlayerHealth)
  currentMonsterHealth = chosenMaxLife
  currentPlayerHealth = chosenMaxLife
  resetGame(chosenMaxLife)
}

const checkGameState = () => {
  const playerDead = currentPlayerHealth <= 0
  const mosterDead = currentMonsterHealth <= 0
  let result
  if (mosterDead && playerDead) {
    result = 'A DRAW'
    alert('You have a draw')
  } else if (mosterDead) {
    result = 'PLAYER WON'
    alert('You won')
  } else if (playerDead) {
    result = 'MONSTER WON'
    alert('You lost!')
  }

  if (playerDead || mosterDead) {
    reset(result)
  }
}

const attackPlayer = () => {
  const playerDamage = dealPlayerDamage(MONSTER_ATTACK_VALUE)
  const initalPlayerHealth = currentPlayerHealth
  currentPlayerHealth -= playerDamage

  if (currentPlayerHealth <= 0 && hasBonusLife) {
    currentPlayerHealth = initalPlayerHealth
    hasBonusLife = false
    removeBonusLife()
    setPlayerHealth(initalPlayerHealth)
    alert('You would have been dead but the bonus life saved you!')
  }
  writeToLog('MONSTER_ATTACK', playerDamage, currentMonsterHealth, currentPlayerHealth, 'PLAYER')
}

const endRound = () => {
  attackPlayer()
  checkGameState()
}

const attackMonster = (damageValue) => {
  const damage = dealMonsterDamage(damageValue)
  currentMonsterHealth -= damage
  return damage
}

const attackHandler = () => {
  const damage = attackMonster(ATTACK_VALUE)
  writeToLog('PLAYER_ATTACK', damage, currentMonsterHealth, currentPlayerHealth, 'MONSTER')
  endRound()
}

const strongAttackHandler = () => {
  const damage = attackMonster(STRONG_ATTACK_VALUE)
  writeToLog('PLAYER_STRONG_ATTACK', damage, currentMonsterHealth, currentPlayerHealth, 'MONSTER')
  endRound()
}

const healPlayerHanlder = () => {
  let healValue
  if (currentPlayerHealth >= chosenMaxLife - HEAL_VALUE) {
    alert("You can't health to more than your max initial health")
    healValue = chosenMaxLife - currentPlayerHealth
  } else {
    healValue = HEAL_VALUE
  }
  currentPlayerHealth += healValue
  increasePlayerHealth(healValue)
  writeToLog('PLAYER_HEAL', healValue, currentMonsterHealth, currentPlayerHealth, 'PLAYER')
  endRound()
}

const printLogHandler = () => {
  console.log('---------------')
  for (const val of battleLog) {
    console.log('{')
    for (const key in val) {
      console.log(`\t${key}: ${val[key]},`)
    }
    console.log('}')
  }
}

attackBtn.addEventListener('click', attackHandler)
strongAttackBtn.addEventListener('click', strongAttackHandler)
healBtn.addEventListener('click', healPlayerHanlder)
logBtn.addEventListener('click', printLogHandler)

adjustHealthBars(chosenMaxLife)
