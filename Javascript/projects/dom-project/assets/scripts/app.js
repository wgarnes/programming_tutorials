const startAddMovieButton = document.getElementById('add-movie-button')
const addMovieModal = document.getElementById('add-modal')
const backdrop = document.getElementById('backdrop')
const cancelAddMovieButton = addMovieModal.querySelector('button.btn.btn--passive')
const confirmAddMovieButton = addMovieModal.querySelector('button.btn.btn--success')
const movieModalInputs = addMovieModal.querySelectorAll('input')
const entryTextSection = document.getElementById('entry-text')
const listRoot = document.getElementById('movie-list')
const deleteMovieModal = document.getElementById('delete-modal')
const cancelDeleteMovieButton = deleteMovieModal.querySelector('button.btn.btn--passive')
const confirmDeleteMovieButton = deleteMovieModal.querySelector('button.btn.btn--danger')
let confirmDeleteMovieHandler = null

const movies = []

const updateUI = () => {
  if (movies.length === 0) {
    entryTextSection.classList.add('visible')
  } else {
    entryTextSection.classList.remove('visible')
  }
}

const deleteMovie = (movie, movieElement) => {
  listRoot.removeChild(movieElement)
  movies.splice(movies.indexOf(movie), 1)
  hideModals()
  updateUI()
}

const deleteMovieHandler = (movie, movieElement) => {
  deleteMovieModal.classList.add('visible')
  backdrop.classList.add('visible')
  confirmDeleteMovieButton.removeEventListener('click', confirmDeleteMovieHandler)
  confirmDeleteMovieHandler = deleteMovie.bind(null, movie, movieElement)
  confirmDeleteMovieButton.addEventListener('click', confirmDeleteMovieHandler)
}

const renderMovieElement = (movie) => {
  const newMovieElement = document.createElement('li')
  newMovieElement.className = 'movie-element'
  newMovieElement.innerHTML = `
    <div class="movie-element__image">
      <img src="${movie['image-url']}" alt="${movie.title}">
    </div>
    <div class="movie-element__info">
      <h2>${movie.title}</h2>
      <p>${movie.rating}/5 stars</p>
    </div>
  `
  listRoot.appendChild(newMovieElement)
  newMovieElement.addEventListener('click', deleteMovieHandler.bind(null, movie, newMovieElement))
}


const hideModals = () => {
  backdrop.classList.remove('visible')
  addMovieModal.classList.remove('visible')
  deleteMovieModal.classList.remove('visible')
  clearMovieModalInputs()
}

const toggleAddMovieModalHandler = () => {
  backdrop.classList.toggle('visible')
  addMovieModal.classList.toggle('visible')
}

const clearMovieModalInputs = () => {
  for (const input of movieModalInputs) {
    input.value = ''
  }
}

const getMovieModalInputs = () => {
  const movie = { valid: false }
  for (const input of movieModalInputs) {
    const value = input.value.trim()
    movie[input.name] = value

    if (value === '') {
      alert('Please enter values for all inputs.')
      return movie
    }
  }

  const rating = movie.rating = +movie.rating
  if(rating < 1 || rating > 5) {
    alert('Please enter a rating between 1 and 5.')
    return movie
  }

  movie.valid = true
  return movie
}

const addMovieHandler = () => {
  const movie = getMovieModalInputs()
  if (!movie.valid) {
    return
  }

  clearMovieModalInputs()
  movies.push(movie)
  console.log(movies)
  toggleAddMovieModalHandler()
  updateUI()
  renderMovieElement(movie)
}


startAddMovieButton.addEventListener('click', toggleAddMovieModalHandler)
cancelAddMovieButton.addEventListener('click', hideModals)
confirmAddMovieButton.addEventListener('click', addMovieHandler)

cancelDeleteMovieButton.addEventListener('click', hideModals)

backdrop.addEventListener('click', hideModals)
