const defaultResult = 0
let currentResult = defaultResult
let logEntries = []

function createAndWriteOutput(operator, operation) {
  const previousValue = currentResult
  const input = +userInput.value
  const description = `${currentResult} ${operator} ${input}`
  currentResult = operation(currentResult, input)
  outputResult(currentResult, description)

  const logEntry = {
    operation: operator,
    previousValue,
    enteredNumber: input,
    newValue: currentResult,
  }

  logEntries.push(logEntry)
  console.log(logEntries)
}

function add() {
  const operation = (val1, val2) => val1 + val2
  createAndWriteOutput('+', operation)
}

function subtract() {
  const operation = (val1, val2) => val1 - val2
  createAndWriteOutput('-', operation)
}

function multiply() {
  const operation = (val1, val2) => val1 * val2
  createAndWriteOutput('*', operation)
}

function divide() {
  const operation = (val1, val2) => val1 / val2
  createAndWriteOutput('/', operation)
}

addBtn.addEventListener('click', add)
subtractBtn.addEventListener('click', subtract)
multiplyBtn.addEventListener('click', multiply)
divideBtn.addEventListener('click', divide)
