const findMax = (...numbers) => {
  return numbers.reduce((previousVal, currentVal) => previousVal > currentVal ? previousVal : currentVal, Number.NEGATIVE_INFINITY)
}

const findMinAndMax = (...numbers) => {
  return numbers.reduce((previousVal, currentVal) => {
    const [min, max] = previousVal
    return [min < currentVal ? min : currentVal, max > currentVal ? max : currentVal]
  }, [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY])
}

const numbers = Array.from(Array(21), (_, index) => (index - 10) * 2)
console.log('numbers = ', numbers)

const greaterThan5 = numbers.filter((number) => number > 5)
console.log('greaterThan5 = ', greaterThan5)

const numbersObject = numbers.map((num) => ({ num }))
console.log('numbersObject = ', numbersObject)

const numbersMultiplied = numbers.reduce((previousVal, currentVal) => previousVal * currentVal, 1)
console.log('numbersMultiplied = ', numbersMultiplied)

console.log(findMax(...numbers))
const [min, max] = findMinAndMax(...numbers)
console.log(`min: ${min}, max: ${max}`)