class Course {
  title
  length
  price

  constructor(title, length, price) {
    this.title = title
    this.length = length
    this.price = price
  }
}

let course1 = new Course('Math', '5 hours', 10.99)
let course2 = new Course('Science', '3 hours', 20.99)

console.log(course1)
console.log(course2)

