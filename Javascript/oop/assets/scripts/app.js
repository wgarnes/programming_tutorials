class Product {
  title = 'DEFAULT'
  imageUrl
  description
  price

  constructor(title, imageUrl, description, price) {
    this.title = title
    this.imageUrl = imageUrl
    this.description = description
    this.price = price
  }
}

class ElementAttribute {
  constructor(attrName, attrValue) {
    this.name = attrName
    this.value = attrValue
  }
}

class Component {
  constructor(renderHook, shouldRender = true) {
    this.hook = renderHook
    if (shouldRender) {
      this.render()
    }
  }

  render() {}

  createRootElement(tag, cssClasses, attributes) {
    const rootElement = document.createElement(tag)
    if (cssClasses) {
      rootElement.className = cssClasses
    }
    attributes?.forEach(({ name, value }) => rootElement.setAttribute(name, value))
    this.hook.appendChild(rootElement)
    return rootElement
  }
}

class ShoppingCart extends Component {
  items = []

  set cartItems(value) {
    this.items = value
    this.totalOutput.innerHTML = `<h2>Total: \$${this.totalAmount}</h2>`
  }

  get totalAmount() {
    return this.items.reduce((prevVal, currItem) => prevVal + currItem.price, 0).toFixed(2)
  }

  constructor(renderHook) {
    super(renderHook)
  }

  addProduct(product) {
    this.cartItems = [...this.items, product]
  }

  orderProducts() {
    console.log('Ordering...')
    console.log(this.items)
  }

  render() {
    const cartEl = this.createRootElement('section', 'cart')
    cartEl.innerHTML = `
    <h2>Total: \$0</h2>
    <button>Order Now!</button>`
    const orderButton = cartEl.querySelector('button')
    orderButton.addEventListener('click', () => this.orderProducts())
    this.totalOutput = cartEl.querySelector('h2')
  }
}

class ProductItem extends Component {
  constructor(product, renderHook) {
    super(renderHook, false)
    this.product = product
    this.render()
  }

  addToCart() {
    App.addProductToCart(this.product)
    console.log('Adding product to cart ...', this.product)
  }

  render() {
    const prodEl = this.createRootElement('li', 'product-item')
    const product = this.product
    prodEl.innerHTML = `
      <div>
        <img src="${product.imageUrl}" alt="${product.title}">
        <div class="product-item__content">
          <h2>${product.title}</h2>
          <h3>\$${product.price}</h3>
          <p>${product.description}</p>
          <button>Add to Cart</button>
        </div>
      </div>
    `
    const addCartBtn = prodEl.querySelector('button')
    addCartBtn.addEventListener('click', this.addToCart.bind(this))
  }
}

class ProductList extends Component {
  #products = []

  constructor(renderHook) {
    super(renderHook)
    this.prodList = this.createRootElement('ul', 'product-list')
    this.#fetchProducts()
  }

  #fetchProducts() {
    this.#products = [
      new Product(
        'A Pillow',
        'https://swissotelathome.com/media/catalog/product/cache/39/image/1000x754/9df78eab33525d08d6e5fb8d27136e95/s/w/swieu-108-s-stp_lrg.jpg',
        'A soft pillow!',
        19.99
      ),
      new Product(
        'A Carpet',
        'http://www.rugrabbit.com/sites/default/files/imagecache/big/knights_oriental_rugs/21-2013/1.jpg',
        'A carpet which you might like!',
        89.99
      ),
    ]
    this.renderProducts()
  }

  renderProducts() {
    for (const product of this.#products) {
      new ProductItem(product, this.prodList)
    }
  }

  render() {
    if (this.prodList && this.#products?.length) {
      this.renderProducts()
    }
  }
}

class Shop extends Component {
  constructor() {
    super()
  }

  render() {
    const renderHook = document.getElementById('app')
    this.cart = new ShoppingCart(renderHook)
    new ProductList(renderHook)
  }
}

class App {
  static cart

  static init() {
    const shop = new Shop()
    this.cart = shop.cart
  }

  static addProductToCart(product) {
    this.cart.addProduct(product)
  }
}

App.init()
