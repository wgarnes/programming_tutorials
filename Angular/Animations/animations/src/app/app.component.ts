import { Component } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes, group } from '@angular/animations';

enum State {
  NORMAL = 'normal',
  HIGHLIGHTED = 'highlighted',
  SHRUNKEN = 'shrunken',
  IN = 'in'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('divState', [
      state(State.NORMAL, style({
        backgroundColor: 'red',
        transform: 'translateX(0)'
      })),
      state(State.HIGHLIGHTED, style({
        backgroundColor: 'blue',
        transform: 'translateX(100px)'
      })),
      transition(`${State.NORMAL} <=> ${State.HIGHLIGHTED}`, animate(300))
    ]),
    trigger('wildState', [
      state(State.NORMAL, style({
        backgroundColor: 'red',
        transform: 'translateX(0) scale(1)'
      })),
      state(State.HIGHLIGHTED, style({
        backgroundColor: 'blue',
        transform: 'translateX(100px) scale(1)'
      })),
      state(State.SHRUNKEN, style({
        backgroundColor: 'green',
        transform: 'translateX(0px) scale(0.5)'
      })),
      transition(`${State.NORMAL} => ${State.HIGHLIGHTED}`, animate(300)),
      transition(`${State.HIGHLIGHTED} => ${State.NORMAL}`, animate(800)),
      transition(`${State.SHRUNKEN} <=> *`, [
        style({
          backgroundColor: 'orange',
          borderRadius: '50px'
        }),
        animate(1000)
      ])
    ]),
    trigger('list1', [
      state(State.IN, style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition(`void => *`, [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(300)
      ]),
      transition(`* => void`, [
        animate(300, style({
          opacity: 0,
          transform: 'translateX(100px)'
        }))
      ])
    ]),
    trigger('list2', [
      state(State.IN, style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition(`void => *`, [
        animate(1000, keyframes([
          style({
            transform: 'translateX(-100px)',
            opacity: 0,
            offset: 0
          }),
          style({
            transform: 'translateX(-50px)',
            opacity: 0.5,
            offset: 0.3
          }),
          style({
            transform: 'translateX(-20px)',
            opacity: 1,
            offset: 0.8
          }),
          style({
            transform: 'translateX(0px)',
            opacity: 1,
            offset: 1
          }),
        ]))
      ]),
      transition(`* => void`, [
        group([
          animate(300, style({
            color: 'red'
          })),
          animate(800, style({
            opacity: 0,
            transform: 'translateX(100px)'
          }))
        ])
      ])
    ])
  ]
})
export class AppComponent {
  state: State = State.NORMAL;
  wildState: State = State.NORMAL;
  list = ['Milk', 'Sugar', 'Bread'];

    onAdd(item) {
      this.list.push(item);
    }

    onDelete(item) {
      this.list.splice(this.list.indexOf(item), 1);
    }

    onAnimate() {
      this.state = this.state === State.NORMAL ? State.HIGHLIGHTED : State.NORMAL;
      this.wildState = this.wildState === State.NORMAL ? State.HIGHLIGHTED : State.NORMAL;
    }

    onShrink() {
      this.wildState = State.SHRUNKEN;
    }

    animationStarted(event) {
      console.log(event);
    }

    animationEnded(event) {
      console.log(event);
    }
}
